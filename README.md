# First rule:

DO NOT ACTUALLY USE THIS CODE TO MAKE INVESTMENT CHOICES. Demonstration only.

# Portfolio Analysis

This code is a demonstration of how one might choose a portfolio
of stocks to meet ones investment goals and tolerance for risk. It
ingests an expected rate of price increase, the variance of that
increase, and the covariance between the different stocks, and then
caculates the expected increase for different combinations of assets.

This code assumes that the rate of change is normally distributed and
that the initial investment is equally split between the assets.

# How to Use

1. pip install -r requirements.txt
2. Update the stock_stats.xlsx excel file as needed. If you add a stock, make sure to add both a row and column to the correlation matrix.
3. python portfolio_eval.py

