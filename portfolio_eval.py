""" This code is a demonstration of how one might choose a portfolio
of stocks to meet ones investment goals and tolerance for risk. It
ingests an expected rate of price increase, the variance of that
increase, and the covariance between the different stocks, and then
caculates the expected increase for different combinations of assets.

This code assumes that the rate of change is normally distributed and
that the initial investment is equally split between the assets.
"""

import pandas as pd
import numpy as np
import scipy.stats as st

def analyze_portfolio(keep_list, mean_var, corr):
    """Calculates the expected return and probability
    of loss for a given combination of assets. Also
    returns the names of those assets.

    Parameters
    ----------
    keep_list : list
        This list provides the combination of assets
        to be assessed. Example [1, 0, 1, 1, 1]

    mean_var : dataframe
        This is the dataframe pulled from the excel
        file and contains data on the mean and
        variance of the assets.

    corr : numpy array
        This array contains the Pearson correlation
        coefficients mapped between the assets. Also
        from the excel file.
    """

    # Turn the keep list into a numpy array and zero out
    # any assets that are not included.
    keep_list = np.array(keep_list)
    sigma = mean_var['std_dev'].to_numpy()
    sigma = sigma * keep_list

    # Calculate the mean return of the included assets.
    mus = mean_var['mean']
    mus = mus * keep_list
    mu_total = mus.sum()

    # Use the asset variance & Pearson correlation matrix
    # to calculate the total asset variance.
    var_total = sigma @ corr @ sigma

    # Now that we have mean & variance, we can calc z-stat
    # and probability of loss.
    z_stat = (0 - mu_total) / (var_total ** .5)
    prob_loss = st.norm.cdf(z_stat)

    # Adjust the mean for the number of assets.
    mu_total = mu_total / keep_list.sum()

    # Return the names of the included assets.
    names = mean_var[['stock']]
    keep_list_tf = [x == 1 for x in keep_list]
    names = names[keep_list_tf]
    names = names['stock'].to_list()

    return [names, mu_total, prob_loss]


if __name__ == '__main__':
    # Import the data from the excel file
    mean_var = pd.read_excel('stock_stats.xlsx', 'mean_variance')
    corr_df = pd.read_excel('stock_stats.xlsx', 'pearson_corr')
    corr = corr_df.drop('stock', axis=1).to_numpy()

    # Initialize variables for looping over all assets
    list_len = mean_var.shape[0]
    results = []

    # Loop over all combination of assets by creating a mask that will
    # be applied to the mean and variance.
    for i in range(2 ** list_len - 1):
        binary = bin(i + 1)
        binary = binary.split('b')[1].rjust(list_len).replace(' ', '0')
        keep_list = [int(x) for x in binary]
        results.append(analyze_portfolio(keep_list, mean_var, corr))

    # Drop the results into a dataframe so we can output the results.
    final_result = pd.DataFrame(
        results,
        columns=['stocks', 'mean_lift', 'prob_of_loss']
    )
    final_result['acceptable'] = np.where(
        final_result['prob_of_loss'] > .15,
        False,
        True,
    )

    final_result.sort_values(['acceptable', 'mean_lift'], ascending=[False, False], inplace=True)

    final_result.to_excel('results.xlsx', index=False)
